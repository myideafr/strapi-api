module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: 'https://strapi.myidea.fr',
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '075c78874c6903e52fc0593db0abc800'),
    },
  },
});
